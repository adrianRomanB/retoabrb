package es.adrianromanb.rsstest.controllers;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;

import es.adrianromanb.rsstest.R;
import es.adrianromanb.rsstest.interfaces.AppInterfaces;
import es.adrianromanb.rsstest.models.RequestNews;
import es.adrianromanb.rsstest.models.ResponseNews;
import es.adrianromanb.rsstest.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by a_ber on 12/06/2017.
 */

public class DataController {

    /**
     * Client Http
     */
    private OkHttpClient client;

    /**
     * Method to GET News
     */
    public void getNews(Context context, RequestNews optionSearch, AppInterfaces.INewsCallback listener){

        String url = Constants.URL_NEWS + Constants.URL_SOURCE + optionSearch.getSource()
                + Constants.URL_SORT + optionSearch.getSort()
                + Constants.URL_API + context.getString(R.string.apikey);

        UrlConnection connection = new UrlConnection();
        connection.execute(url, listener);

    }

    /**
     * URLConection
     */
    private class UrlConnection extends AsyncTask<Object, Void, String>{

        private AppInterfaces.INewsCallback listener;

        @Override
        protected String doInBackground(Object... params) {

            listener = (AppInterfaces.INewsCallback)params[1];
            client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url((String)params[0])
                    .build();

            Response response = null;
            String body = null;
            try {
                response = client.newCall(request).execute();
                if(response.body()!=null){
                    body = response.body().string();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return body;
        }

        protected void onPostExecute(String response){
            Log.d("RssTest", response);
            ResponseNews responseNews = new Gson().fromJson(response, ResponseNews.class);

            if(responseNews!=null){
                listener.getData(responseNews);
            }else{
                listener.getError();
            }
        }
    }
}
