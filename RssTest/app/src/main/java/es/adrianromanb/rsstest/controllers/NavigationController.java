package es.adrianromanb.rsstest.controllers;

import es.adrianromanb.rsstest.fragments.MainFragment;
import es.adrianromanb.rsstest.fragments.RSSDetailFragment;
import es.adrianromanb.rsstest.fragments.RSSListFragment;
import es.adrianromanb.rsstest.interfaces.AppInterfaces;

/**
 * Created by a_ber on 12/06/2017.
 */

public class NavigationController {

    /**
     * Fragment
     */
    private MainFragment fragment;

    /**
     * Application State
     */
    private static int state;

    public void initNavigation(AppInterfaces.IChangeFragment listener){
        //create fragment
        fragment = new RSSListFragment();

        //go to listener
        listener.initView(fragment);
    }

    /**
     * Method to Navigate between fragments
     */
    public void pushFragment(int option, AppInterfaces.IChangeFragment listener){
        switch (option){
            case 1:
                fragment = new RSSDetailFragment();
                break;
            default:
                fragment = new RSSListFragment();
                break;
        }
        listener.initView(fragment);
    }
}
