package es.adrianromanb.rsstest.application;

import android.app.Application;

import es.adrianromanb.rsstest.models.RSSNews;

/**
 * Created by a_ber on 12/06/2017.
 */

public class RSSTest extends Application {

    /**
     * RSSNew Selected
     */
    private static RSSNews rssNewSelected;

    public static RSSNews getRssNewSelected() {
        return rssNewSelected;
    }

    public static void setRssNewSelected(RSSNews rssNewSelected) {
        RSSTest.rssNewSelected = rssNewSelected;
    }
}
