package es.adrianromanb.rsstest.models;

/**
 * Created by a_ber on 12/06/2017.
 */

public class RequestNews {

    /**
     * Source
     */
    private String source;

    /**
     * Sort
     */
    private String sort;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
