package es.adrianromanb.rsstest.utils;

/**
 * Created by a_ber on 12/06/2017.
 */

public class Constants {

    public static final String URL_NEWS = "https://newsapi.org/v1/articles";
    public static final String URL_SOURCE = "?source=";
    public static final String URL_SORT = "&sortBy=";
    public static final String URL_API = "&apiKey=";
}
