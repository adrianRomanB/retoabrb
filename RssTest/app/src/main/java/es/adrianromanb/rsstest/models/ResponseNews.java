package es.adrianromanb.rsstest.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by a_ber on 12/06/2017.
 */

public class ResponseNews {

    /**
     * Status
     */
    @SerializedName("status")
    private String status;

    /**
     * Source
     */
    @SerializedName("source")
    private String source;

    /**
     * Sort
     */
    @SerializedName("sortBy")
    private String sortBy;

    /**
     * Articles
     */
    @SerializedName("articles")
    private ArrayList<RSSNews> articles;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public ArrayList<RSSNews> getArticles() {
        return articles;
    }

    public void setArticles(ArrayList<RSSNews> articles) {
        this.articles = articles;
    }
}
