package es.adrianromanb.rsstest.activities;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import es.adrianromanb.rsstest.R;
import es.adrianromanb.rsstest.controllers.DataController;
import es.adrianromanb.rsstest.controllers.NavigationController;
import es.adrianromanb.rsstest.fragments.MainFragment;
import es.adrianromanb.rsstest.interfaces.AppInterfaces;

public class MainActivity extends AppCompatActivity {

    /**
     * Actual fragment
     */
    private MainFragment fragment;

    /**
     * Params from other module
     */
    private Bundle params;

    /**
     * Navigation Controller
     */
    private NavigationController controller;

    /**
     * Data Controller
     */
    private DataController dataController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controller = new NavigationController();
        dataController = new DataController();

        controller.initNavigation(new AppInterfaces.IChangeFragment() {
            @Override
            public void initView(MainFragment fragment) {
                changeFragment(fragment);
            }
        });
    }

    public void onBackPressed(){
        if(fragment!=null){
            fragment.onBackPressed();
        }else{
            super.onBackPressed();
        }
    }

    /**
     * Method to change fragment
     * @param fragment
     */
    public void changeFragment (MainFragment fragment){

        //get fragments manager
        FragmentManager fragmentManager = getSupportFragmentManager();

        if(this.fragment!=null){
            //remove fragment
            fragmentManager.beginTransaction().remove(this.fragment).commit();
        }
        //save fragment
        this.fragment = fragment;

        //set arguments
        //If there are params
        if(getIntent().getExtras()!=null){
            //save params
            params = getIntent().getExtras();
            fragment.setArguments(params);
        }


        //if fragment not null
        if (fragment != null) {
            //set fragment
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();

        }
    }

    /**
     * Method to GET NavigationController
     */
    public NavigationController getController(){
        return this.controller;
    }

    /**
     * Method to GET DataController
     */
    public DataController getDataController(){
        return this.dataController;
    }
}
