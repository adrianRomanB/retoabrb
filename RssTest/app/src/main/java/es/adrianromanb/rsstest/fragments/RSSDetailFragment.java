package es.adrianromanb.rsstest.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import es.adrianromanb.rsstest.R;
import es.adrianromanb.rsstest.activities.MainActivity;
import es.adrianromanb.rsstest.application.RSSTest;
import es.adrianromanb.rsstest.interfaces.AppInterfaces;
import es.adrianromanb.rsstest.models.RSSNews;

/**
 * Created by a_ber on 12/06/2017.
 */

public class RSSDetailFragment extends MainFragment {

    /**
     * RSSNew
     */
    private RSSNews rssNew;

    /**
     * Image View
     */
    private ImageView image;

    /**
     * Title
     */
    private TextView title;

    /**
     * Description
     */
    private TextView description;

    /**
     * Button
     */
    private Button mainButton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rss_detail_fragment, container, false);

        //set Views
        setViews(view);

        //getInfo
        setInfo();

        //set Listeners
        setListeners();

        return view;
    }

    public void onBackPressed() {
        ((MainActivity) getActivity()).getController().pushFragment(0, new AppInterfaces.IChangeFragment() {
            @Override
            public void initView(MainFragment fragment) {
                ((MainActivity) getActivity()).changeFragment(fragment);
            }
        });
    }

    /**
     * Method to set Views
     *
     * @param view
     */
    private void setViews(View view) {
        image = (ImageView) view.findViewById(R.id.item_image);
        title = (TextView) view.findViewById(R.id.item_title);
        description = (TextView) view.findViewById(R.id.item_description);
        mainButton = (Button) view.findViewById(R.id.main_button);
    }

    /**
     * Method to set Listeners
     */
    private void setListeners() {
        mainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rssNew.getUrl() != null) {
                    Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(rssNew.getUrl()));
                    getActivity().startActivity(browser);
                }
            }
        });
    }

    /**
     * Method to set Info
     */
    private void setInfo() {
        rssNew = RSSTest.getRssNewSelected();

        if (rssNew != null) {
            if (rssNew.getImage() != null) {
                Glide.with(getActivity()).load(rssNew.getImage()).into(image);
            }

            if (rssNew.getTitle() != null) {
                title.setText(rssNew.getTitle());
            }

            if (rssNew.getDescription() != null) {
                description.setText(rssNew.getDescription());
            }
        }
    }
}
