package es.adrianromanb.rsstest.models;

import com.google.gson.annotations.SerializedName;

/**
 * RSS News Model
 */

public class RSSNews {

    /**
     * Title
     */
    @SerializedName("title")
    private String title;

    /**
     * Description
     */
    @SerializedName("description")
    private String description;

    /**
     * Image URL
     */
    @SerializedName("urlToImage")
    private String image;

    /**
     * Author
     */
    @SerializedName("author")
    private String author;

    /**
     * URL to WebSite
     */
    @SerializedName("url")
    private String url;

    /**
     * Published
     */
    @SerializedName("publishedAt")
    private String date;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
