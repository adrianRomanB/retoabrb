package es.adrianromanb.rsstest.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import es.adrianromanb.rsstest.R;
import es.adrianromanb.rsstest.models.RSSNews;

/**
 * Created by a_ber on 12/06/2017.
 */

public class RSSAdapter extends BaseAdapter implements Filterable {

    /**
     * Full List
     */
    private ArrayList<RSSNews> fullList;

    /**
     * List to show
     */
    private ArrayList<RSSNews> showList;

    /**
     * Value Filter
     */
    private ValueFilter valueFilter;

    /**
     * View Holder
     */
    private Holder holder;

    /**
     * Context
     */
    private Context context;

    /**
     * Inflater
     */
    private LayoutInflater inflater;

    public RSSAdapter(Context context, ArrayList<RSSNews> rssNews){
        // save list
        this.fullList = rssNews;
        this.showList = rssNews;

        //save context
        this.context = context;

        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return showList.size();
    }

    @Override
    public Object getItem(int position) {
        return showList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = inflater.inflate(R.layout.list_item, parent, false);

            holder = new Holder();
            holder.imageItem = (ImageView)convertView.findViewById(R.id.item_image);
            holder.titleItem = (TextView)convertView.findViewById(R.id.item_title);
            holder.descItem = (TextView)convertView.findViewById(R.id.item_description);

            convertView.setTag(holder);
        }else{
            holder = (Holder)convertView.getTag();
        }

        if(showList.get(position).getTitle()!=null){
            holder.titleItem.setText(showList.get(position).getTitle());
        }

        if(showList.get(position).getDescription()!=null){
            holder.descItem.setText(showList.get(position).getDescription());
        }

        if(showList.get(position).getImage()!=null){
            Glide.with(context).load(showList.get(position).getImage()).into(holder.imageItem);
        }

        return convertView;
    }

    /**
     * View Holder
     */
    private class Holder{
        //set Items to show
        ImageView imageItem;
        TextView titleItem;
        TextView descItem;
    }

    @Override
    public Filter getFilter() {
        if(valueFilter == null) {
            valueFilter = new ValueFilter();
        }
        return valueFilter;
    }

    /**
     * Method to Filter
     */
    private class ValueFilter extends Filter {
        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results=new FilterResults();
            if(constraint!=null && constraint.length()>0){
                ArrayList<RSSNews> filterList=new ArrayList<>();
                for(int i=0;i<fullList.size();i++){
                    if ((fullList.get(i).getTitle().toUpperCase())
                            .contains(constraint.toString().toUpperCase())) {
                        RSSNews rssNews = fullList.get(i);
                        filterList.add(rssNews);
                    }

                }
                results.count = filterList.size();
                results.values = filterList;
            }else{
                results.count = fullList.size();
                results.values = fullList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            showList =(ArrayList<RSSNews>) results.values;
            notifyDataSetChanged();
        }
    }


}
