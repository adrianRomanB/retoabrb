package es.adrianromanb.rsstest.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import es.adrianromanb.rsstest.R;
import es.adrianromanb.rsstest.activities.MainActivity;
import es.adrianromanb.rsstest.adapters.RSSAdapter;
import es.adrianromanb.rsstest.application.RSSTest;
import es.adrianromanb.rsstest.interfaces.AppInterfaces;
import es.adrianromanb.rsstest.models.RSSNews;
import es.adrianromanb.rsstest.models.RequestNews;
import es.adrianromanb.rsstest.models.ResponseNews;

/**
 * Created by a_ber on 12/06/2017.
 */

public class RSSListFragment extends MainFragment {

    /**
     * Rss News
     */
    private ArrayList<RSSNews> arrayList;

    /**
     * ListView
     */
    private ListView listView;

    /**
     * Adapter
     */
    private RSSAdapter adapter;

    /**
     * Search
     */
    private EditText search;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rss_list_fragment, container, false);

        //set Views
        setViews(view);

        //getInfo
        setInfo();

        //set Listeners
        setListeners();

        return view;
    }

    /**
     * Method onBackPressed
     */
    public void onBackPressed() {
        getActivity().finish();
    }

    /**
     * Method to set Views
     *
     * @param view
     */
    private void setViews(View view) {

        search = (EditText) view.findViewById(R.id.search);
        listView = (ListView) view.findViewById(R.id.list_form);

    }

    /**
     * Method to set Listeners
     */
    private void setListeners() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                RSSTest.setRssNewSelected(arrayList.get(position));
                ((MainActivity) getActivity()).getController().pushFragment(1, new AppInterfaces.IChangeFragment() {
                    @Override
                    public void initView(MainFragment fragment) {
                        ((MainActivity) getActivity()).changeFragment(fragment);
                    }
                });
            }
        });

        search.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                if (adapter != null) {
                    if (arg0 != null && arg0.length() > 0) {
                        adapter.getFilter().filter(arg0);
                    }
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
    }

    /**
     * Method to set Info
     */
    private void setInfo() {
        RequestNews options = new RequestNews();
        options.setSource("the-next-web");
        options.setSort("latest");

        ((MainActivity) getActivity()).getDataController().getNews(getActivity(), options, new AppInterfaces.INewsCallback() {
            @Override
            public void getData(ResponseNews responseNews) {

                Log.d("RssTEST", "RESPUESTA CORRECTA");
                if (responseNews != null && responseNews.getArticles() != null) {
                    arrayList = responseNews.getArticles();
                    adapter = new RSSAdapter(getActivity(), arrayList);
                    listView.setAdapter(adapter);
                }
            }

            @Override
            public void getError() {
                Log.d("RssTEST", "ERROR EN LA LLAMADA");
            }
        });
    }
}
