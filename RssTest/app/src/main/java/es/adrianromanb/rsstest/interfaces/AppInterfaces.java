package es.adrianromanb.rsstest.interfaces;


import es.adrianromanb.rsstest.fragments.MainFragment;
import es.adrianromanb.rsstest.models.ResponseNews;

/**
 * Created by a_ber on 12/06/2017.
 */

public class AppInterfaces {

    /**
     * Interface for changeFragment
     */
    public interface IChangeFragment{
        void initView(MainFragment fragment);
    }

    /**
     * News Response
     */
    public interface INewsCallback{
        void getData(ResponseNews responseNews);
        void getError();
    }
}
